# -*- coding: utf-8 -*-

from dateutil.relativedelta import relativedelta
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class Prestamo(models.Model):
    _name = "prestamo"
    _description = "Tabla de prestamos"

    name = fields.Char("Nombre", compute="_compute_name")
    partner_id = fields.Many2one("res.partner", "Partner")
    date = fields.Date("Fecha", default=fields.Date.today())
    fees = fields.Integer("Cantidad de cuotas")
    amount_total = fields.Float("Deuda")
    line_ids = fields.One2many("prestamo.line", "prestamo_id", "Cuotas")
    status = fields.Selection(
        [('draft', 'Borrador'), ('done', "Hecho")], 
        "Status",
        default="draft"
    )

    @api.model
    def name_get(self):
        return ((prestamo.id, f"Prestamo #{prestamo.id}") for prestamo in self)

    @api.depends("partner_id", "date")
    def _compute_name(self):
        for rec in self:
            # Puede ser encapsulado en un ternario, pero queda la linea muy larga
            if rec.partner_id and rec.date:
                rec.name = f"Prestamo de {rec.partner_id.name} en la fecha {rec.date}."
            else:
                rec.name = ""

    def action_create_line_ids(self):
        if not all(field for field in (self.date, self.fees, self.amount_total)):
            raise ValidationError(
                _("Algunos campos son obligatorios, llenelos antes de proceder")
            )

        for i in range(1, self.fees + 1):
            self.line_ids += self.line_ids.create({
                "amount": self.amount_total / self.fees,
                "payment_date": self.date + relativedelta(months=i)
            })

        self.status = "done"

    @api.constrains("fees", "amount_total")
    def _check_negative_fields(self):
        for rec in self:
            if any(f < 0 for f in (rec.fees, rec.amount_total)):
                raise ValidationError(_("Ni la cantidad de cuotas ni la deuda pueden ser negativa"))

class PrestamoLine(models.Model):
    _name = "prestamo.line"
    _description = "Lineas de los prestamos"

    payment_date = fields.Date("Fecha tope")
    amount = fields.Float("Monto a pagar")
    prestamo_id = fields.Many2one("prestamo")
