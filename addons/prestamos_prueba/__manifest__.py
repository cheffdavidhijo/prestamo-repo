# -*- coding: utf-8 -*-
{
    'name': "prestamos_prueba",
    'description': "Modulo de test",
    'author': "David Linarez",
    'category': 'Accounting',
    'version': '1.0',
    'depends': ['base', 'contacts', 'account'],
    'data': [
        'security/res_groups.xml',
        'security/ir.model.access.csv',
        'views/views.xml',
    ],
    "application": False,
    "installable": True
}
